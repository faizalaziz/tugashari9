<?php
require_once "animal.php";
require_once "ape.php";
require_once "frog.php";

$sheep = new Animal("shaun");
$sungokong = new Ape("kera sakti");
$kodok = new Frog("buduk");

$sheep->get_info();
echo "<br>";

$kodok->get_info();
echo "Jump : " ; $kodok->jump() ;
echo "<br><br>";

$sungokong->get_info();
echo "Yell : " ; $sungokong->yell() ; 
echo "<br><br>";


?>